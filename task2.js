const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputURL = "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=";
const outputFile = "output3.json";

var output = {}
console.log("getting 10 acronyms");
output.definitions = [];
console.log("creating looping function")

const getAcronym = async () => {
  let acronym = randomstring.generate(3).toUpperCase();
  let response;
  try {
    console.log("got data for acronym", acronym);
    console.log("add returned data to definitions array");
    response = await got(inputURL + acronym)
    output.definitions.push(response.body);

  } catch (err) {
    console.log(err)
  }

  if (output.definitions.length < 10) {
    console.log("calling looping function again");
    await getAcronym();
  }
}

(async () => {
  console.log("calling looping function");
  await getAcronym();
  console.log("saving output file formatted with 2 space indenting");
  jsonfile.writeFile(outputFile, output, { spaces: 2 }, function (err) {
    console.log("All done!");
  })
  console.log('end')
})()
