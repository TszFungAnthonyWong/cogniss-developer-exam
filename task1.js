const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const outputFile = "output2.json";


const data = require('./input2.json');

let newdata = {}

newdata.emails = data.names.map(item => {
    let rand = randomstring.generate(5);
    let str = item.split("").reverse().join("") + rand + '@gmail.com';
    return str
})

jsonfile.writeFile(outputFile, newdata, { spaces: 2 }, function (err) {
    console.log("All done!");
})